from threading import Thread
from dataCtrl import models
import time
import random

TCT_FILE_NAME = {
            'T240809':['HSIL'],
            'T240803':['LSIL'],
            'T240811':[],
            'T240813':[],
            'T242490':[],
            'T243267':['SCC','鳞癌']
        }

#先is_delete=False，再is_diagnostic=2，50秒后再is_diagnostic=1
class TheadSetTslideDiagnose(Thread):
    def __init__(self,file_name:str):
        self.file_name = file_name
        Thread.__init__(self)
    def run(self) -> None:
        tslide = models.TSlide.objects.filter(slide_file_name=self.file_name).first()
        tslide.is_small_slide = True #默认都是小切片
        name ,_ = self.file_name.split('.')
        if not tslide:
            print('没有文件')
            return
        #tct切片是is_delete=True,is_diagnostic=None, 未点击上传的
        if tslide.is_diagnostic==None and tslide.is_delete==True:
            tslide.is_delete = False
            tslide.is_diagnostic = 2
            tslide.save()
            print(self.file_name, '设置成计算中')
            time.sleep(50 + random.randint(1, 5))
            print(self.file_name, '设置成已完成智能诊断')
            tslide.is_diagnostic = 1
            tslide.save()
        else:
            #tct切片是is_delete=False，有智能诊断结果的，将智能诊断的逻辑再走一遍
            if name in TCT_FILE_NAME:
                tslide.is_diagnostic = 2
                tslide.save()
                print(self.file_name,'设置成计算中')
                time.sleep(50+random.randint(1,5))
                print(self.file_name,'设置成已完成智能诊断')
                tslide.is_diagnostic = 1
                tslide.save()

def set_slide_delete_not_diagnose(file_name):
    tslide = models.TSlide.objects.filter(slide_file_name=file_name).first()
    name, _ = file_name.split('.')
    if name in TCT_FILE_NAME:
        tslide.is_delete=True
        tslide.is_diagnostic=None

        tslide.save()
