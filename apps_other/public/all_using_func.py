from PIL import Image
import os

#colon 结肠镜是jpg格式的数据，不能通过ai那边生成缩略图，由我们自己生成
def convert_jpg_to_bmp_thumbnail(jpg_path, thumbnail_size=(128, 128)):
    '''
    切片的路径，把切片图片进行缩略图操作，存在跟切片同一个路径中，名字都是固定好的
    '''
    try:
        bmp_save_path = os.path.join(os.path.dirname(jpg_path),'preview.bmp')
        # 打开JPG图片
        with Image.open(jpg_path) as img:
            # 创建缩略图
            img.thumbnail(thumbnail_size)
            # 保存为BMP格式
            img.save(bmp_save_path, "BMP")
        return True
    except Exception:
        return False
