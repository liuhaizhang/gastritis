from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
import json

class NewResponse(HttpResponse):
    """
    An HTTP response class that consumes data to be serialized to JSON.

    :param data: Data to be dumped into json. By default only ``dict`` objects
      are allowed to be passed due to a security flaw before EcmaScript 5. See
      the ``safe`` parameter for more information.
    :param encoder: Should be a json encoder class. Defaults to
      ``django.core.serializers.json.DjangoJSONEncoder``.
    :param safe: Controls if only ``dict`` objects may be serialized. Defaults
      to ``True``.
    :param json_dumps_params: A dictionary of kwargs passed to json.dumps().
    """

    def __init__(self,code=200,msg='操作成功', data=None,error=None,pagination = None, **kwargs):
        ret_data = {
            'code':code,
            'msg':msg
        }

        if error:
            ret_data['code']=400
            ret_data['error']=error
            if 'data' in ret_data:
                ret_data.pop('data')
        if data:
            ret_data['code']=201
            ret_data['data']=data
            if 'error' in ret_data:
                ret_data.pop('error')
        if pagination:
            ret_data=pagination

        if kwargs:
            for k,v in kwargs.items():
                if k not in ['error','data','code','msg']:
                    ret_data[k]=v

        kwargs.setdefault('content_type', 'application/json')
        str_ret_data = json.dumps(ret_data,ensure_ascii=False)
        super().__init__(content=str_ret_data, **kwargs)