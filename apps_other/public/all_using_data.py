import os

#apps_other中的应用，图片存的总目录
PUBLIC_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)),'upload')

#结肠镜系统的图片的存放总路径
COLON_PATH = os.path.join(PUBLIC_PATH,'colon')
#结肠镜系统的智能诊断的截图存放的路径
COLON_DIAGNOSE_path = os.path.join(COLON_PATH,'diagnose')


if __name__ == '__main__':
    print(PUBLIC_PATH,'总静态文件路径')
    print(COLON_PATH,'结肠镜总路径')
    print(COLON_DIAGNOSE_path,'结肠镜智能诊断的路径')