from threading import Thread
from dataCtrl import models
import time
import random
from datetime import datetime,timedelta
from django.core.cache import cache

#1、设置tct切片的状态：先is_delete=False，再is_diagnostic=2（智能诊断计算中），50秒后再is_diagnostic=1（智能诊断完成）
class TheadSetTslideDiagnose(Thread):
    def __init__(self,file_name:str,user_id:int,cache_key:str):
        self.file_name = file_name
        self.user_id = user_id
        self.cache_key = cache_key
        Thread.__init__(self)
    def run(self) -> None:
        tslide = models.TSlide.objects.filter(slide_file_name=self.file_name).first()
        tslide.is_small_slide = True #默认都是小切片
        name ,_ = self.file_name.split('.')
        diagnoise_dic = {
            'all_files': 1,  # 要进行智能诊断的所有文件数量
            'finiship_files': 0,  # 完成智能诊断的文件数量
            'complete': 0,  # 所有文件是否完成了智能诊断
            # 'file_list': None,  # 要进行智能诊断的文件列表
            'all_counts': 10,  # 一个文件生成四个文件才是完成，总文件数x4
            'finiship_counts': 0,  # 一个文件总四次
        }
        #将智能诊断结果写到cache中
        cache.set(self.cache_key,diagnoise_dic)
        if not tslide:
            print('没有文件')
            return
        #tct切片是is_delete=True,is_diagnostic=None, web端点击上传时
        if tslide.is_diagnostic==None and tslide.is_delete==True:
            #1、记录到历史上传表中
            obj = models.UserWebUploadFileName.objects.filter(
                user_id=self.user_id,
                file_name=self.file_name,
            ).first()
            exp_datetime = datetime.today() + timedelta(days=7)
            if obj:
                obj.exp_datetime = exp_datetime.strftime('%Y-%m-%d %H:%M:%S')
                obj.save()
            else:
                models.UserWebUploadFileName.objects.create(
                    user_id=self.user_id,file_name=self.file_name,
                    exp_datetime=exp_datetime.strftime('%Y-%m-%d %H:%M:%S')
                )
            #2、设置切片的状态
            tslide.is_delete = False
            tslide.is_diagnostic = 2
            tslide.save()
            print(self.file_name, '设置成计算中')
            #3、在者50多秒中
            for i in range(0,50):
                if i %5==0:
                    time.sleep(5)
                    diagnoise_dic['finiship_counts'] += 1
                    #将智能诊断结果更新到cache中
                    cache.set(self.cache_key,diagnoise_dic)
            else:
                time.sleep(random.randint(2,5))
                diagnoise_dic['finiship_counts'] = 10
                diagnoise_dic['complete'] = 1
                diagnoise_dic['finiship_files'] = 1
                # 将智能诊断结果更新到cache中
                cache.set(self.cache_key, diagnoise_dic)

            #3、等待时间结束后，设置智能诊断成功
            print(self.file_name, '设置成已完成智能诊断')
            tslide.is_diagnostic = 1
            tslide.save()
        else:
            # 同名下，智能诊断过了，且不是删除状态下
            #1、先设置成计算中
            tslide.is_diagnostic = 2
            tslide.save()
            print(self.file_name,'设置成计算中')
            #2、模拟智能诊断过程，50多秒完成智能诊断
            for i in range(1, 50):
                if i % 5 == 0:
                    time.sleep(5)
                    diagnoise_dic['finiship_counts'] += 1
                    # 将智能诊断结果更新到cache中
                    cache.set(self.cache_key, diagnoise_dic)
            else:
                time.sleep(random.randint(2, 5))
                diagnoise_dic['finiship_counts'] = 10
                diagnoise_dic['complete'] = 1
                diagnoise_dic['finiship_files'] = 1
                # 将智能诊断结果更新到cache中
                cache.set(self.cache_key, diagnoise_dic)
            #3、智能诊断结束，将切片的智能诊断状态改为完成
            print(self.file_name,'设置成已完成智能诊断')
            tslide.is_diagnostic = 1
            tslide.save()
