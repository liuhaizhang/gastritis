from django.apps import AppConfig


class ColonConfig(AppConfig):
    name = 'apps_other.colon'
