from django.shortcuts import render

from apps_other.public.response import NewResponse
from public import global_common as gcm

# Create your views here.

@gcm.check_api_token()
def test_func(request):
    return NewResponse(msg='测试成功')


