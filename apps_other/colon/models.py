from django.db import models
from django.db.models import Count
# Create your models here.

#切片表
class TSlide(models.Model):
    slide_id = models.AutoField(primary_key=True)
    slide_file_name = models.CharField(max_length=100, blank=True, null=True) #切片名
    slide_path = models.CharField(max_length=500, blank=True, null=True) #切片路径
    create_time = models.DateTimeField(blank=True, null=True, auto_now_add=True) #切片上传时间
    is_diagnostic = models.IntegerField(blank=True, null=True) #2023-02-06，null=未处理，数值1=智能诊断了，2=计算中
    scanning_time = models.DateTimeField('扫描时间', blank=True, null=True,auto_now_add=True)
    is_delete = models.BooleanField(default=False,verbose_name='web上传文件，重名覆盖，标记删除')#是否删除，上传时，同名覆盖，就设置删除掉
    diagnose_time = models.DateTimeField(null=True) #最近一次智能诊断时间

    confirm_account = models.ForeignKey(to='user.TAccount', on_delete=models.SET_NULL, db_column='confirm_account', blank=True,
                                        null=True,related_name='colon_slide',related_query_name='colon_slide')
    class Meta:
        managed = True
        db_table = 'colon_t_slide'


#智能诊断结果表：截图+部位+诊断结果
class TSlideImage(models.Model):
    slide_image_id = models.AutoField(primary_key=True)
    path = models.CharField(max_length=255, blank=True, null=True)
    '''2023-02-28:GPU智能诊断接口修改，新增2个字段part和result'''
    part = models.IntegerField(verbose_name='诊断部分',null=True)
    #1萎缩性，肠上皮化生，活动性，炎症
    result = models.IntegerField(verbose_name='诊断结果',null=True)
    #0无、1轻度、2中度、3重度
    #说明，一个切片进行新的智能诊断接口处理后，会产生四张图片，每张图片对应一个诊断部分和诊断结果。


    slide = models.ForeignKey(TSlide, models.DO_NOTHING, blank=True, null=True)
    class Meta:
        managed = True
        db_table = 'colon_t_slide_image'


class TSlideDiagnose(models.Model):
    slide_diagnose_id = models.AutoField(primary_key=True)
    rise = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    gender = models.CharField(max_length=255, blank=True, null=True)
    age = models.CharField(max_length=255, blank=True, null=True)
    number = models.CharField(max_length=255, blank=True, null=True)#病历号
    department = models.CharField(max_length=255, blank=True, null=True)
    hospital = models.CharField(max_length=255, blank=True, null=True)
    part = models.CharField(max_length=255, blank=True, null=True)
    content = models.CharField(max_length=255, blank=True, null=True)
    diagnose = models.CharField(max_length=255, blank=True, null=True)
    doctor = models.CharField(max_length=255, blank=True, null=True)

    create_time = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    #0主系统 1 萎缩系统 2 胃炎系统 3 活动性系统 4 肠上皮化生系统
    system = models.IntegerField(blank=True, null=True)
    #新增2022/12/06，在settings.SLIDE_TYPE.get(1) 拿到对应的中文
    slide_type = models.SmallIntegerField(default=1,verbose_name='暂时就是这个{1:"CT/活检"}')
    # 病理诊断中整体诊断的结果
    check_entirety_result=models.CharField(max_length=512,default='无',verbose_name="医生手动输入诊断结果")
    #新增2022/12/06， part=1 在settings.PART_OF_SELECTION.get(part)[1]拿到对应的中文，level=1,在settings.DIAGNOSE_LEVEL.get(level) 拿到对应的中文
    #新增2022/12/06，病理中，各个部位的诊断结果
    check_part_result = models.TextField(null=True,verbose_name="[{'part':胃体,'level':胃体的诊断情况,'note':'备注情况'},]")

    #2023-03-07诊断报告新增 显著可见、诊断部位（四个，[{'part':'','level'}]）、详解、打印次数、病历号
    clearly_visible = models.CharField(max_length=255,null=True,verbose_name='显著可见')
    four_part_result = models.CharField(max_length=255,null=True,verbose_name='[{"part":1,"result":1}]') #看public/web_upload_public.py下两个常量
    detailed_explanation = models.CharField(max_length=512,null=True,verbose_name='详解')
    print_count = models.IntegerField(default=0,verbose_name='打印次数')
    medical_number = models.CharField(max_length=128,null=True,verbose_name='病历号')#多的字段，没有用，病历号是上面的number

    slide = models.ForeignKey(TSlide, models.DO_NOTHING, blank=True, null=True)
    image = models.ManyToManyField(TSlideImage, through='SlideDiagnoseImage')
    '''
    切片号=切片外键.slide_file_name.split('.')[0]
    送检时间=切片外键.create_time
    '''

    class Meta:
        managed = True
        db_table = 'colon_t_slide_diagnose'

#智能诊断的四个图片
class SlideDiagnoseImage(models.Model):
    id = models.AutoField(primary_key=True)
    slide_diagnose = models.ForeignKey(TSlideDiagnose, models.PROTECT)
    slide_image = models.ForeignKey(TSlideImage, models.PROTECT)
    class Meta:
        managed = True
        db_table = 'colon_t_slide_diagnose_image'

#2023-08-24新增，记录用户上传的所有文件名， 前端在展示用户上传的文件时，实时展示文件切割、文件智能诊断时需要
class UserWebUploadFileName(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.IntegerField(verbose_name='用户的id')
    file_name = models.CharField(max_length=256,verbose_name='上传过的文件名',null=True)
    file_rename = models.CharField(max_length=256,verbose_name='上传的文件名重名了,重新设置的名字',null=True)  #2023-08-25 16：48新增
    create_date = models.DateField(auto_now_add=True,verbose_name='创建记录的日期')
    create_datetime = models.DateTimeField(auto_now_add=True,verbose_name='创建记录时间')
    exp_datetime = models.DateTimeField(verbose_name='只查询在这个时间内的文件,创建时间+7天',null=True)
    is_diagnose = models.CharField(max_length=16,default='yes',verbose_name='该切片选择了智能诊断') #yes 或 no
    class Meta:
        managed = True
        db_table = 'colon_t_user_upload_file'